package mulXSubY;

import java.util.ArrayList;
import java.util.Iterator;

public class MinSeq {
	private int ops = 0;
	private int X;
	private int Y;
	private ITree<Integer> tree;
	private ArrayList<Integer> numbers;
	private int b;
	private boolean stop = false;
	
	/**
	 *  All values must be integers.
	 * @param X value to multiply by, cannot be 0
	 * @param Y value to substract by, cannot be 0
	 * @param a starting number
	 * @param b	ending number
	 */
	public MinSeq(int X, int Y, int a, int b) {
		tree = new Tree<Integer>(a);
		numbers = new ArrayList<Integer>();
		numbers.add(a);
		this.X = X;
		this.Y =Y;
		this.b = b;
	}
	/**
	 * 
	 * @return returns the number of operations needed to solve, returns 0 if the number calculations exceed a certain limit.
	 */
	public int calc() {
		while(!stop) {
			ops++;
			for (TreePosition<Integer> pos : tree.positions()) {
				if(pos.numberOfChildren() == 0 && pos.getElement() != null) {
					addChildren(pos);
				}
			}
		}
		
		return ops;
	}
	
	/** adds 2 children to the parent node, one with the multiplied value, the other with the substracted value.
	 * Also checks if the child value is already in the tree, if so then remove this child. 
	 * 
	 * @param parent, the parent which to add the children to
	 */
	private void addChildren(TreePosition<Integer> parent) {
		tree.attach(parent, new Tree<Integer>(parent.getElement()*X));
		tree.attach(parent, new Tree<Integer>(parent.getElement()-Y));
		@SuppressWarnings("unchecked")
		Iterator<TreePosition<Integer>> i = (Iterator<TreePosition<Integer>>) parent.getChildren().iterator();
		while (i.hasNext()) {
			TreePosition<Integer> child = i.next(); // must be called before you can call i.remove()
			if(child.getElement() == b) {
				stop = true;
			}
			if(Math.abs(child.getElement()) > Math.abs(b*X*X*X*Y*Y*Y)) {
				ops = 0;
				stop = true;
			}
			boolean remove = false;
			for (Integer number : numbers) {
				if(number == child.getElement()) {
					remove = true;
				}
			}
			if (remove) {
				i.remove();
			}
			else {
				numbers.add(child.getElement());
			}
		}
	}
}
