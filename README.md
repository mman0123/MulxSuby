This project uses an algorithm to find the minimum sequence of numbers to get from a number to a number only by multiplying by X or subtracting by Y.
The underlying data structure used is a binary tree. If a number in this binary tree gets too big (bigger than ((X^3)*(Y^3)) the returned sequence of numbers will be 0.
Also if a new number was already in the binary tree, this node gets deleted to make sure the algorithm doesn't calculate the same numbers over and over again.


